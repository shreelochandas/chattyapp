import 'package:agora_rtm/agora_rtm.dart';
import 'package:chatty_app/configFile/config.dart';
import 'package:chatty_app/ui/msg/message.dart';
import 'package:flutter/material.dart';

class RtmDataProvider extends ChangeNotifier {
  final AgoraRtmClient _rtmClient;

  RtmDataProvider(this._rtmClient);

  bool _isRtmLoggedIn = false;
  bool _isInChannel = false;
  String _rtmErrorMessage = '';
  AgoraRtmChannel _rtmChannel;
  List<String> _listOfRtmChannels = [];
  String _channelMessage = '';

  // List<Message> _receivedChannelMessages = [];
  Map<String, List<Message>> _allChats = Map();

  bool get isRtmLoggedIn => _isRtmLoggedIn;

  bool get isInChannel => _isInChannel;

  String get rtmErrorMessage => _rtmErrorMessage;

  AgoraRtmChannel get rtmChannel => _rtmChannel;

  List<String> get listOfRtmChannels => _listOfRtmChannels;

  String get channelMessage => _channelMessage;

  // List<Message> get receivedChannelMessages => _receivedChannelMessages;

  Map<String, List<Message>> get allChats => _allChats;

  void setChannelMessage({@required String message}) {
    _channelMessage = message;
    notifyListeners();
  }

  Future<void> agoraLogIn({@required String userId}) async {
    if (_isRtmLoggedIn == true) {
      try {
        await _rtmClient.logout();
        _isRtmLoggedIn = false;
        print('Rtm logout successful');
      } catch (errorCode) {
        _rtmErrorMessage = 'Rtm client logout failed.';
        print('Rtm client logout error: $errorCode');
      }
    }

    String _token =
    userId == 'userone@gmail.com' ? Config.rtmToken01 : Config.rtmToken02;

    try {
      await _rtmClient.login(_token, userId);
      _isRtmLoggedIn = true;
      print('Rtm login successful');
    } catch (errorCode) {
      _rtmErrorMessage = 'Rtm client login failed.';
      print('Rtm client login error: $errorCode');
    }
  }

  Future<String> createChannel({@required String name}) async {
    if (name == null || name == '') {
      _rtmErrorMessage = 'Please input channel id to join.';
      notifyListeners();
      return '';
    } else {
      try {
        _rtmChannel = await _rtmClient.createChannel(name);
        _allChats[name] = [];
        _rtmChannel.onMessageReceived =
            (AgoraRtmMessage message, AgoraRtmMember member) {
          // _receivedChannelMessages[member.userId] = message.text;
          // _receivedChannelMessages
          //     .add(Message(sender: member.userId, text: message.text));
          _allChats[member.channelId]
              .add(Message(sender: member.userId, text: message.text));
          // print(_receivedChannelMessages);
        };
        _listOfRtmChannels.add(name);
        print('Rtm channel creation successful');
        notifyListeners();
        return name;
      } catch (errorCode) {
        _rtmErrorMessage = 'Rtm channel creation failed.';
        print('Rtm channel creation failed.');
        notifyListeners();
        return '';
      }
    }
  }

  Future<String> createExistingChannel({@required String name}) async {
    if (name == null || name == '') {
      _rtmErrorMessage = 'Please input channel id to join.';
      notifyListeners();
      return '';
    } else {
      try {
        _rtmChannel = await _rtmClient.createChannel(name);
        print('Rtm channel creation successful');
        notifyListeners();
        return name;
      } catch (errorCode) {
        _rtmErrorMessage = 'Rtm channel creation failed.';
        print('Rtm channel creation failed.');
        notifyListeners();
        return '';
      }
    }
  }

  Future<void> joinChannel({@required AgoraRtmChannel rtmChannel}) async {
    if (_isInChannel == true) {
      await leaveChannel();
    }

    try {
      await _rtmChannel.join();
      _isInChannel = true;
      print('Rtm channel joining successful');
    } catch (errorCode) {
      _rtmErrorMessage = 'Rtm channel joining failed.';
      print('Rtm channel joining failed.');
    }
  }

  Future<void> leaveChannel() async {
    if (_isInChannel == true) {
      try {
        await _rtmChannel.leave();
        _rtmClient.releaseChannel(_rtmChannel.channelId);
        _isInChannel = false;
        print('Rtm channel leave successful');
      } catch (errorCode) {
        _rtmErrorMessage = 'Rtm channel leave failed.';
      }
    }
    notifyListeners();
  }

  Future<void> sendChannelMessage({@required String sender}) async {
    if (_channelMessage == null || _channelMessage == '') {
      _rtmErrorMessage = 'Please input text to send.';
    } else {
      try {
        await _rtmChannel
            .sendMessage(AgoraRtmMessage.fromText(_channelMessage));
        // _receivedChannelMessages
        //     .add(Message(sender: sender, text: _channelMessage));
        _allChats[_rtmChannel.channelId]
            .add(Message(sender: sender, text: _channelMessage));
        print(_channelMessage);
        print('Send channel message success.');
      } catch (errorCode) {
        _rtmErrorMessage = 'Sending channel message failed.';
        print('Sending channel message failed.');
      }
    }
    notifyListeners();
  }
}
