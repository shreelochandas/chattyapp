import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class AuthenticationDataProvider extends ChangeNotifier {
  final FirebaseAuth _firebaseAuth;

  AuthenticationDataProvider(this._firebaseAuth);

  bool _isSignedIn = false;
  bool _dataConnectionAvailable = true;
  bool _ongoingOperation = false;
  String _authErrorMessage = '';
  Map<String, String> _userDetails = Map();

  bool get isSignedIn => _isSignedIn;

  bool get dataConnectionAvailable => _dataConnectionAvailable;

  bool get ongoingOperation => _ongoingOperation;

  String get authErrorMessage => _authErrorMessage;

  Map<String, String> get userDetails => _userDetails;

  void addUserDetails({@required String key, @required String value}) {
    _userDetails[key] = value;
    notifyListeners();
  }

  Future<void> getDataConnectionStatus() async {
    _dataConnectionAvailable = await DataConnectionChecker().hasConnection;
    notifyListeners();
  }

  void setDataConnectionAvailableToTrue() {
    _dataConnectionAvailable = true;
    notifyListeners();
  }

  Future<void> signInUser(
      {@required String email, @required String password}) async {
    await getDataConnectionStatus();
    if (_dataConnectionAvailable == true && _ongoingOperation == false) {
      _ongoingOperation = true;
      try {
        final UserCredential userCredential = await _firebaseAuth
            .signInWithEmailAndPassword(email: email, password: password);
        _ongoingOperation = false;
        _isSignedIn = true;
        _userDetails.clear();
        _userDetails['userEmail'] = userCredential.user.email;
        _userDetails['userId'] = userCredential.user.uid;
        print(_userDetails);
      } on FirebaseAuthException catch (error) {
        _ongoingOperation = false;
        _isSignedIn = false;
        _authErrorMessage = error.message;
        print(error.message);
      }
    }
    notifyListeners();
  }

  Future<void> signUpUser(
      {@required String email, @required String password}) async {
    await getDataConnectionStatus();
    if (_dataConnectionAvailable == true && _ongoingOperation == false) {
      _ongoingOperation = true;
      try {
        final UserCredential userCredential = await _firebaseAuth
            .createUserWithEmailAndPassword(email: email, password: password);
        _ongoingOperation = false;
        _isSignedIn = true;
        _userDetails.clear();
        _userDetails['userEmail'] = userCredential.user.email;
        _userDetails['userId'] = userCredential.user.uid;
        print(_userDetails);
      } on FirebaseAuthException catch (error) {
        _ongoingOperation = false;
        _isSignedIn = false;
        _authErrorMessage = error.message;
        print(error.message);
      }
    }
    notifyListeners();
  }
}
