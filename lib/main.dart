import 'package:agora_rtm/agora_rtm.dart';
import 'package:chatty_app/core/providers/providerAuth.dart';
import 'package:chatty_app/core/providers/rtmProvider.dart';
import 'package:chatty_app/ui/totalRoutes/appRouter.dart';
import 'package:chatty_app/configFile/config.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

Future<void> main() async {
  AgoraRtmClient _rtmClient;
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  _rtmClient = await AgoraRtmClient.createInstance(Config.rtmAppId);
  runApp(MyApp(
    rtmClient: _rtmClient,
  ));
}

class MyApp extends StatelessWidget {
  final AgoraRtmClient rtmClient;

  const MyApp({Key key, this.rtmClient}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<AuthenticationDataProvider>(
            create: (context) =>
                AuthenticationDataProvider(FirebaseAuth.instance)),

        ChangeNotifierProvider<RtmDataProvider>(
            create: (context) => RtmDataProvider(rtmClient)),
      ],
      child: MaterialApp(
        title: 'Chatty',
        initialRoute: Config.routeInit,
        onGenerateRoute: AppRouter.generateRoutes,
      ),
    );
  }
}
