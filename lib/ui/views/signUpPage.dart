import 'package:chatty_app/core/providers/providerAuth.dart';
import 'package:chatty_app/core/providers/rtmProvider.dart';
import 'package:chatty_app/configFile/config.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';

import '../../configFile/config.dart';

class SignUpView extends StatelessWidget {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  // _showSnackBar({@required BuildContext context, @required String message}) {
  //   ScaffoldMessenger.of(context).showSnackBar(
  //     SnackBar(
  //       backgroundColor: Color(0xff0B0E28),
  //       content: Text(message),
  //       action: SnackBarAction(
  //         label: 'CLOSE',
  //         onPressed: () {
  //           ScaffoldMessenger.of(context).hideCurrentSnackBar();
  //         },
  //       ),
  //     ),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    final _authenticationServiceProvider =
    Provider.of<AuthenticationDataProvider>(context, listen: false);

    final _rtmDataProvider =
    Provider.of<RtmDataProvider>(context, listen: false);

    return Scaffold(
      backgroundColor: Colors.deepOrangeAccent,
      resizeToAvoidBottomPadding: false,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(
              left: 25.0, right: 25.0, top: 25.0, bottom: 25.0),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(
                    left: 25.0, right: 25.0, top: 50.0, bottom: 25.0),
                child: Text(
                  'Register',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: Config.font1,
                      fontSize: 56.0,
                      color: Colors.white,
                      fontWeight: FontWeight.normal),
                ),
              ),
              SizedBox(
                height: 50.0,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  controller: emailController,
                  decoration: InputDecoration(
                    labelText: 'Email',
                    labelStyle: TextStyle(
                      color: Colors.white,
                      fontFamily: Config.font2,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  controller: passwordController,
                  decoration: InputDecoration(
                    labelText: 'password',
                    labelStyle: TextStyle(
                      color: Colors.white,
                      fontFamily: Config.font1,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  obscureText: true,
                ),
              ),
              // Container(
              //   height: MediaQuery.of(context).size.height / 10,
              //   width: MediaQuery.of(context).size.width,
              //   child: Expanded(
              //     child: Consumer<AuthenticationDataProvider>(
              //         builder: (context, authenticationService, child) {
              //       if (_authenticationServiceProvider.ongoingOperation ==
              //           true) {
              //         return Center(
              //           child: SpinKitThreeBounce(
              //             color: Color(0xff0B0E28),
              //             size: 30.0,
              //           ),
              //         );
              //       } else {
              //         return Center();
              //       }
              //     }),
              //   ),
              // ),
              SizedBox(
                height: 50.0,
              ),
              ElevatedButton(
                style: ButtonStyle(
                  backgroundColor:
                  MaterialStateProperty.all<Color>(Color(0xff0B0E28)),
                ),
                onPressed: () async {
                  await _authenticationServiceProvider.signUpUser(
                      email: emailController.text,
                      password: passwordController.text);

                  if (_authenticationServiceProvider.isSignedIn == true) {
                    await _rtmDataProvider.agoraLogIn(
                        userId: _authenticationServiceProvider
                            .userDetails['userEmail']);

                    if (_rtmDataProvider.isRtmLoggedIn == true) {
                      Navigator.pushReplacementNamed(context, Config.routeHome);
                    } else {
                      Fluttertoast.showToast(
                          msg: _rtmDataProvider.rtmErrorMessage,
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.TOP,
                          timeInSecForIosWeb: 1,
                          backgroundColor: Colors.red,
                          textColor: Colors.white,
                          fontSize: 16.0
                      );

                      print('hello11');
                    }
                  } else if (_authenticationServiceProvider
                      .dataConnectionAvailable ==
                      false) {
                    Fluttertoast.showToast(
                        msg: "Internet is not connected",
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.TOP,
                        timeInSecForIosWeb: 1,
                        backgroundColor: Colors.red,
                        textColor: Colors.white,
                        fontSize: 16.0
                    );
                    print('hello12');
                  } else {
                    print('hello13');
                    Fluttertoast.showToast(
                        msg: _authenticationServiceProvider.authErrorMessage,
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.TOP,
                        timeInSecForIosWeb: 1,
                        backgroundColor: Colors.red,
                        textColor: Colors.white,
                        fontSize: 16.0
                    );

                  }
                },
                child: Text(
                  'Register',
                  style: TextStyle(
                    fontFamily: Config.font1,
                    fontSize: 19.0,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.all(15.0),
                child: Row(
                  children: [
                    Text(
                      "Already have an account?",
                      style: TextStyle(
                          fontFamily: Config.font1,
                          fontSize: 17.0,
                          fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      width: 5.0,
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.pushReplacementNamed(
                            context, Config.routeSignIn);
                      },
                      child: Text(
                        'Login',
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: Config.font2,
                            fontSize: 17.0,
                            fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
