import 'package:chatty_app/core/providers/providerAuth.dart';
import 'package:chatty_app/configFile/config.dart';
import 'package:chatty_app/core/providers/rtmProvider.dart';
import 'package:chatty_app/ui/msg/messages.dart';
import 'package:chatty_app/ui/msg/message.dart';
import 'package:provider/provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChatChannelView extends StatelessWidget {
  final messageController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final _rtmDataProvider =
    Provider.of<RtmDataProvider>(context, listen: true);
    final _authenticationServiceProvider =
    Provider.of<AuthenticationDataProvider>(context, listen: true);



    return Scaffold(
      appBar: AppBar(
        title: Text(
          '${_rtmDataProvider.rtmChannel.channelId}',
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontFamily: Config.font1,
              letterSpacing: 2.0),
        ),
        actions: [
          IconButton(
            icon: Icon(
              CupertinoIcons.clear,
            ),
            onPressed: () async {
              await _rtmDataProvider.leaveChannel();
              Navigator.pop(context);
            },
          ),
        ],

        centerTitle: true,
        backgroundColor: Colors.purple,
      ),
      body: SafeArea(
        child: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            return Column(
              children: [
                Consumer<RtmDataProvider>(
                  builder: (context, authenticationService, child) {
                    return Container(
                      width: constraints.maxWidth,
                      height: (constraints.maxHeight / 100) * 80,

                      child: ListView.builder(
                        itemCount: _rtmDataProvider
                            .allChats[_rtmDataProvider.rtmChannel.channelId]
                            .length,
                        itemBuilder: (BuildContext context, int index) {
                          List<Message> _reversedMessageList = _rtmDataProvider
                              .allChats[_rtmDataProvider.rtmChannel.channelId];

                          bool _isMe = _authenticationServiceProvider
                              .userDetails['userEmail'] ==
                              _rtmDataProvider.allChats[
                              _rtmDataProvider.rtmChannel.channelId]
                                  .elementAt(index)
                                  .sender
                              ? true
                              : false;

                          return MessageBubble(
                            sender:
                            _reversedMessageList.elementAt(index).sender,
                            text: _reversedMessageList.elementAt(index).text,
                            isMe: _isMe,
                          );
                        },
                      ),
                    );
                  },
                ),
                Container(
                  padding: EdgeInsets.only(
                    left: 20.0,
                    right: 20.0,
                  ),
                  height: (constraints.maxHeight / 100) * 20,
                  width: constraints.maxWidth,
                  child: Row(
                    children: [
                      Expanded(
                        child: TextField(
                          decoration: InputDecoration(
                            labelText: 'Type your message here',
                          ),
                          controller: messageController,
                          onChanged: (value) {
                            _rtmDataProvider.setChannelMessage(message: value);
                          },
                        ),
                      ),
                      SizedBox(
                        width: 20.0,
                      ),
                      ElevatedButton(
                        onPressed: () async {
                          messageController.clear();
                          await _rtmDataProvider.sendChannelMessage(
                              sender: _authenticationServiceProvider
                                  .userDetails['userEmail']);
                        },
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(
                              Colors.purple),
                        ),
                        child: Text('Send'),
                      ),
                    ],
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}

/*
Column(
          children: [
            Container(
              height: (MediaQuery.of(context).size.height / 100) * 90,
              width: MediaQuery.of(context).size.width,
              color: Colors.cyan,
            ),
            Container(
              height: (MediaQuery.of(context).size.height / 100) * 10,
              width: MediaQuery.of(context).size.width,
            ),
          ],
        ),
 */
