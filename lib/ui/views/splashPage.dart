import 'package:chatty_app/core/providers/providerAuth.dart';
import 'package:chatty_app/core/providers/rtmProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../configFile/config.dart';

class AuthWrapperView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _authenticationServiceProvider =
    Provider.of<AuthenticationDataProvider>(
      context,
      listen: false,
    );

    WidgetsBinding.instance.addPostFrameCallback(
          (timeStamp) {
        Future.delayed(
          const Duration(seconds: 2),
              () => {
            if (_authenticationServiceProvider.isSignedIn)
              {Navigator.pushReplacementNamed(context, Config.routeHome)}
            else
              {Navigator.pushReplacementNamed(context, Config.routeSignIn)}
          },
        );
      },
    );

    return Scaffold(
      backgroundColor: Colors.blueAccent,
      body: Center(
        child: Text(
          'Chatty',
          style: TextStyle(
            color: Colors.white,
            fontSize: 55.0,
            fontWeight: FontWeight.bold,
            fontFamily: Config.font1,
          ),
        ),
      ),
    );
  }
}
