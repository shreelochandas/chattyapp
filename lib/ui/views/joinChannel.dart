import 'package:chatty_app/core/providers/rtmProvider.dart';
import 'package:chatty_app/configFile/config.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:fluttertoast/fluttertoast.dart';

class JoinChannelView extends StatelessWidget {
  final TextEditingController channelIdController = TextEditingController();



  @override
  Widget build(BuildContext context) {
    final _rtmDataProvider =
    Provider.of<RtmDataProvider>(context, listen: false);

    return Scaffold(
      backgroundColor: Colors.blueAccent,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text(
          'Chatty',
          style: TextStyle(
              fontFamily: Config.font1,
              // fontSize: 32.0,
              fontWeight: FontWeight.bold,
              letterSpacing: 2.0),
        ),
        centerTitle: true,
        // toolbarHeight: 100.0,
        backgroundColor: Colors.purple,
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(
              left: 25.0, right: 25.0, top: 25.0, bottom: 25.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(
                    left: 25.0, right: 25.0, top: 50.0, bottom: 25.0),
                child: Text(
                  'Join Channel',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: Config.font1,
                      fontSize: 35.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(
                height: 50.0,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  controller: channelIdController,
                  decoration: InputDecoration(
                    labelText: 'Channel ID',
                    labelStyle: TextStyle(
                      color: Colors.white,
                      fontFamily: Config.font2,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
              ),

              SizedBox(
                height: 50.0,
              ),
              ElevatedButton(
                style: ButtonStyle(
                  backgroundColor:
                  MaterialStateProperty.all<Color>(Color(0xff0B0E28)),
                ),
                onPressed: () async {
                  print(_rtmDataProvider.listOfRtmChannels
                      .contains(channelIdController.text));
                  if (_rtmDataProvider.listOfRtmChannels
                      .contains(channelIdController.text)) {
                    Fluttertoast.showToast(
                        msg: 'Already exists',
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.TOP,
                        timeInSecForIosWeb: 1,
                        backgroundColor: Colors.red,
                        textColor: Colors.white,
                        fontSize: 16.0
                    );
                    print('hello8');
                  } else {
                    String _channelId = await _rtmDataProvider.createChannel(
                        name: channelIdController.text);

                    if (_channelId == '') {
                      print('hello9');
                      Fluttertoast.showToast(
                          msg: _rtmDataProvider.rtmErrorMessage,
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.TOP,
                          timeInSecForIosWeb: 1,
                          backgroundColor: Colors.red,
                          textColor: Colors.white,
                          fontSize: 16.0
                      );
                    } else {
                      await _rtmDataProvider.joinChannel(
                          rtmChannel: _rtmDataProvider.rtmChannel);

                      if (_rtmDataProvider.isInChannel) {
                        Navigator.pushReplacementNamed(
                            context, Config.routeChatChannel);
                      } else {
                        print('hello10');
                        Fluttertoast.showToast(
                            msg: _rtmDataProvider.rtmErrorMessage,
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.TOP,
                            timeInSecForIosWeb: 1,
                            backgroundColor: Colors.red,
                            textColor: Colors.white,
                            fontSize: 16.0
                        );
                      }
                    }
                  }
                },
                child: Text(
                  'Join Channel',
                  style: TextStyle(
                    fontFamily: Config.font2,
                    fontSize: 19.0,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              SizedBox(
                height: 150.0,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
