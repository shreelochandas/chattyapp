import 'package:chatty_app/core/providers/providerAuth.dart';
import 'package:chatty_app/configFile/config.dart';
import 'package:chatty_app/core/providers/rtmProvider.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../configFile/config.dart';

class SignInView extends StatelessWidget {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();



  @override
  Widget build(BuildContext context) {
    final _authenticationServiceProvider =
    Provider.of<AuthenticationDataProvider>(context, listen: false);

    final _rtmDataProvider =
    Provider.of<RtmDataProvider>(context, listen: false);

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.deepOrangeAccent,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(
              left: 25.0, right: 25.0, top: 25.0, bottom: 25.0),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(
                    left: 25.0, right: 25.0, top: 50.0, bottom: 25.0),
                child: Text(
                  'Login Now',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: Config.font1,
                      fontSize: 56.0,
                      color: Colors.white,
                      fontWeight: FontWeight.normal),
                ),
              ),
              SizedBox(
                height: 50.0,
              ),
              Padding(
                padding: const EdgeInsets.all(7.0),
                child: TextField(
                  controller: emailController,
                  decoration: InputDecoration(
                    labelText: 'Enter Email',
                    labelStyle: TextStyle(
                      color: Colors.white,
                      fontFamily: Config.font2,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  controller: passwordController,
                  decoration: InputDecoration(
                    labelText: 'password',
                    labelStyle: TextStyle(
                      color: Colors.white,
                      fontFamily: Config.font2,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  obscureText: true,
                ),
              ),

              SizedBox(
                height: 50.0,
              ),
              ElevatedButton(
                style: ButtonStyle(
                  backgroundColor:
                  MaterialStateProperty.all<Color>(Color(0xff0B0E28)),
                ),
                onPressed: () async {
                  await _authenticationServiceProvider.signInUser(
                      email: emailController.text,
                      password: passwordController.text);

                  if (_authenticationServiceProvider.isSignedIn == true) {
                    await _rtmDataProvider.agoraLogIn(
                        userId: _authenticationServiceProvider
                            .userDetails['userEmail']);
                    if (_rtmDataProvider.isRtmLoggedIn == true) {
                      Navigator.pushReplacementNamed(context, Config.routeHome);
                    } else {
                      Fluttertoast.showToast(
                          msg:  _rtmDataProvider.rtmErrorMessage,
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.TOP,
                          timeInSecForIosWeb: 1,
                          backgroundColor: Colors.red,
                          textColor: Colors.white,
                          fontSize: 16.0
                      );
                      print('hello1');
                    }
                  } else if (_authenticationServiceProvider
                      .dataConnectionAvailable ==
                      false) {
                    print('hello2');
                    Fluttertoast.showToast(
                        msg: "Internet is not connected",
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.TOP,
                        timeInSecForIosWeb: 1,
                        backgroundColor: Colors.red,
                        textColor: Colors.white,
                        fontSize: 16.0
                    );

                  } else {
                    print('hello3');
                    Fluttertoast.showToast(
                        msg: "The email address is badly formatted.",
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.TOP,
                        timeInSecForIosWeb: 1,
                        backgroundColor: Colors.red,
                        textColor: Colors.white,
                        fontSize: 16.0
                    );
                  }
                },
                child: Text(
                  'Login',
                  style: TextStyle(
                    fontFamily: Config.font2,
                    fontSize: 19.0,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.all(15.0),
                child: Row(
                  children: [
                    Text(
                      "Don't have an account?",
                      style: TextStyle(
                        fontFamily: Config.font2,
                        fontWeight: FontWeight.w500,
                        fontSize: 17.0,
                      ),
                    ),
                    SizedBox(
                      width: 5.0,
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.pushReplacementNamed(
                            context, Config.routeSignUp);
                      },
                      child: Text(
                        'Register',
                        style: TextStyle(
                            fontSize: 17.0,
                            color: Colors.white,
                            fontFamily: Config.font2,
                            fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
