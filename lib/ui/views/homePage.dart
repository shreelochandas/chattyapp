import 'package:chatty_app/core/providers/rtmProvider.dart';
import 'package:chatty_app/configFile/config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';

class HomeView extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    final _rtmDataProvider =
    Provider.of<RtmDataProvider>(context, listen: false);

    return Scaffold(
      backgroundColor: Colors.blueAccent,
      appBar: AppBar(
        title: Text(
          'Chatty',
          style: TextStyle(
              fontFamily: Config.font1,
              // fontSize: 32.0,
              fontWeight: FontWeight.bold,
              letterSpacing: 2.0),
        ),
        centerTitle: true,
        // toolbarHeight: 100.0,
        backgroundColor: Colors.purple,
      ),
      body: Consumer<RtmDataProvider>(
        builder: (BuildContext context, value, Widget child) {
          if (_rtmDataProvider.listOfRtmChannels.isEmpty == true) {
            return Center(
              child: Text(
                'Create room to chat.',
                style: TextStyle(
                    color: Colors.white,
                    fontFamily: Config.font2,
                    fontSize: 18.0,
                    fontWeight: FontWeight.w600),
              ),
            );
          } else {
            return Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.all(15.0),
              child: ListView.builder(
                itemCount: _rtmDataProvider.listOfRtmChannels.length,
                itemBuilder: (BuildContext context, int index) {
                  return Padding(
                    padding: const EdgeInsets.only(bottom: 15.0),
                    child: ListTile(
                      onTap: () async {
                        await _rtmDataProvider.leaveChannel();
                        String _channelId =
                        await _rtmDataProvider.createExistingChannel(
                            name: _rtmDataProvider.listOfRtmChannels
                                .elementAt(index));

                        if (_channelId == '') {
                          print('hello6');
                          Fluttertoast.showToast(
                              msg: _rtmDataProvider.rtmErrorMessage,
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.TOP,
                              timeInSecForIosWeb: 1,
                              backgroundColor: Colors.red,
                              textColor: Colors.white,
                              fontSize: 16.0
                          );

                        } else {
                          await _rtmDataProvider.joinChannel(
                              rtmChannel: _rtmDataProvider.rtmChannel);

                          if (_rtmDataProvider.isInChannel) {
                            Navigator.pushNamed(
                                context, Config.routeChatChannel);
                          } else {
                            print('hello7');
                            Fluttertoast.showToast(
                                msg: _rtmDataProvider.rtmErrorMessage,
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.TOP,
                                timeInSecForIosWeb: 1,
                                backgroundColor: Colors.red,
                                textColor: Colors.white,
                                fontSize: 16.0
                            );

                          }
                        }
                      },
                      tileColor: Color(0xffF7F7F9),
                      title: Text(
                        _rtmDataProvider.listOfRtmChannels.elementAt(index),
                        style: TextStyle(
                          fontFamily: Config.font2,
                          fontWeight: FontWeight.w600,
                          fontSize: 18.0,
                        ),
                      ),
                    ),
                  );
                },
              ),
            );
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        elevation: 0.0,
        backgroundColor: Colors.deepOrange,
        onPressed: () {
          Navigator.pushNamed(context, Config.routeActions);
        },
        child: Icon(
          Icons.add,
        ),
      ),
    );
  }
}

