import 'package:chatty_app/core/providers/rtmProvider.dart';
import 'package:chatty_app/configFile/config.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ActionsView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _rtmDataProvider =
    Provider.of<RtmDataProvider>(context, listen: true);

    return Scaffold(
      backgroundColor: Colors.blueAccent,
      appBar: AppBar(
        title: Text(
          'Chatty',
          style: TextStyle(
              fontFamily: Config.font1,
              // fontSize: 32.0,
              fontWeight: FontWeight.bold,
              letterSpacing: 2.0),
        ),
        centerTitle: true,
        // toolbarHeight: 100.0,
        backgroundColor: Colors.purple,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            RaisedButton(
              color: Colors.purple,
              onPressed: (){},
              child: GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, Config.routeCreateChannel);
                },
                child: Text(
                  'Create Channel',
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: Config.font2,
                      fontSize: 23.0,
                      fontWeight: FontWeight.w600),
                ),
              ),
            ),
            SizedBox(
              height: 30.0,
            ),
            RaisedButton(
              color: Colors.purple,
              onPressed: (){},
              child: GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, Config.routeJoinChannel);

                },
                child: Text(
                  'Join Channel',
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: Config.font2,
                      fontSize: 23.0,
                      fontWeight: FontWeight.w600),
                ),
              ),
            ),
            // SizedBox(
            //   height: 30.0,
            // ),
            // Text(
            //   'Profile',
            //   style: TextStyle(
            //       fontFamily: Config.fontMontserrat,
            //       fontSize: 23.0,
            //       fontWeight: FontWeight.w600),
            // ),
          ],
        ),
      ),
    );
  }
}
