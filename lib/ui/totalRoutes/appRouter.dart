import 'package:chatty_app/ui/views/createJoin.dart';
import 'package:chatty_app/ui/views/splashPage.dart';
import 'package:chatty_app/ui/views/chatDashboard.dart';
import 'package:chatty_app/ui/views/createChannel.dart';
import 'package:chatty_app/ui/views/homePage.dart';
import 'package:chatty_app/ui/views/joinChannel.dart';
import 'package:chatty_app/ui/views/signInPage.dart';
import 'package:chatty_app/ui/views/signUpPage.dart';
import 'package:chatty_app/configFile/config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppRouter {
  static Route<dynamic> generateRoutes(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case (Config.routeAuthWrapper):
        return MaterialPageRoute(builder: (BuildContext context) => AuthWrapperView());
      case (Config.routeSignIn):
        return MaterialPageRoute(
            builder: (BuildContext context) => SignInView());
      case (Config.routeSignUp):
        return MaterialPageRoute(
            builder: (BuildContext context) => SignUpView());
      case (Config.routeHome):
        return MaterialPageRoute(
            builder: (BuildContext context) => HomeView());
      case (Config.routeChatChannel):
        return MaterialPageRoute(
            builder: (BuildContext context) => ChatChannelView());
      case (Config.routeActions):
        return MaterialPageRoute(
            builder: (BuildContext context) => ActionsView());
      case (Config.routeCreateChannel):
        return MaterialPageRoute(
            builder: (BuildContext context) => CreateChannelView());
      case (Config.routeJoinChannel):
        return MaterialPageRoute(
            builder: (BuildContext context) => JoinChannelView());
      default:
        return MaterialPageRoute(
            builder: (BuildContext context) => Scaffold(
              body: Center(
                child: Text('No route defined for ${routeSettings.name}'),
              ),
            ));
    }
  }
}
