class Config {
  // Routes
  static const String routeAuthWrapper = '/';
  static const String routeSignIn = '/sign-in';
  static const String routeSignUp = '/sign-up';
  static const String routeHome = '/home';
  static const String routeChatChannel = '/chat-channel';
  static const String routeActions = '/actions';
  static const String routeCreateChannel = '/create-channel';
  static const String routeJoinChannel = '/join-channel';
  static const String routeInit = routeAuthWrapper;

  // Fonts
  static const String font1 = 'Ubuntu';
  static const String font2 = 'Rubik';


  // Rtm
  static const String rtmAppId = '4d10c894ad8648f9bcce5d76317076c1';
  static const String rtmToken01 =
      '0064d10c894ad8648f9bcce5d76317076c1IABae87yeLflY4bqiKh/cWHwPP0RU0VstLruqidrmWnKLP/INHQAAAAAEACtzyAs62oAYAEA6ANraiRi';
  static const String rtmToken02 =
      '0064d10c894ad8648f9bcce5d76317076c1IACi7gtkGjSfII8PjgSnhBYgWG5BQktUZ2vXKM2DPCX4sG9e5FQAAAAAEADl8dWfO3EAYAEA6AO7cCRi';


  // Colour
  static const int colorPrimary = 0xff0B0E28;
}
